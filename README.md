# ScapersPi Aquarium Controller

**Important**: This design is not fully tested. Before using it I reccomend a 
thorough review. The features I tested worked as intended but this is not a 
guarantee that all features will or that the hardware won't break.

![](images/scaperspi_3.jpg)

This is a prototype of a aquarium controller. It's based on a Raspberry Pi Zero W.
The planned features were:

- 5 fertilizer channels. Compatible with Jecod slave 4 channel dosing pump.
- 4 LED dimming channels
- 6 general purpose PWM channels
- 6 buffered 5 V general purpose IOs
- 2 1wire connectors
- A 1wire PCB-temperature sensor
- [A watchdog](#attiny-watchdog) monitoring the Raspberry Pi and rebooting it if needed


In [this video](https://www.youtube.com/watch?v=sQLH_Cj07GI) I demonstrate the 
manual fertilization function of the controller. The board used is the one 
tagged `rev_0.1` in this repo. **Note**: `rev_0.1` has a too small inductor on 
the voltage regulator. The current version fixes this. However the current version is untested!

I'll upload the Software used in this demo shortly.

## ATtiny Watchdog
The board includes a watchdog based on an ATtiny. It monitors an I2C message sent 
from the Pi. If that message isn't received, the watchdog cuts the power to the 
Pi for a short duration and hence forces a reboot. The software and further 
documentation can be found in [this repo](https://gitlab.com/marcelkr/attiny-watchdog).
